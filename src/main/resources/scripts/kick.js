function voteKick() {
    if ($size >= 2) {
        var name = $args[1];
        var target = Bukkit.getPlayer(name);
        if (target === null) {
            $sender.sendMessage(name + " is not online!");
            return;
        }
        var kickVote = $ctx.setVote(10, // 10 seconds
            "%votes% votes to Kick " + name + " from the game", // initial broadcast message
            "%votes%/%online% to remove " + name, // The message to broadcast when someone votes
            function (passed, voters) {
                if (passed) {
                    if (target !== null) {
                        $ctx.sendCommand("kick " + name + " You were vote kicked!")
                    }
                    Bukkit.broadcastMessage(name + " was kicked from the server!");
                } else {
                    Bukkit.broadcastMessage("Not enough votes were made!");
                }

                Bukkit.broadcastMessage('Finished!');
            });
        kickVote.start();
    } else {
        $sender.sendMessage("Invalid arguments: /partyvote kick (player name)")
    }
}

voteKick();