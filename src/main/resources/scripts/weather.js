function sendCmd(command) {
    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
}

function worldEnvironmentVote() {
    if ($size >= 2) {
        var option = $args[1];
        var description = "";
        var onVoteBroadcast = "";
        switch (option.toLowerCase()) {
            case "day":
            case "night":
                description = "A vote to change the time to: " + option + " was made!";
                onVoteBroadcast = "%votes%/%online% voted to change the time to " + option;
                break;

            case "clear":
            case "rain":
            case "snow":
            case "lightning":
                description = "A vote to change the weather to: " + option + " was made!";
                onVoteBroadcast = "%votes%/%online% voted to change the weather to " + option;
                break;
            default:
                print("Invalid");
                return
        }

        var timeVote = $ctx.setVote(10, description, onVoteBroadcast, function (passed, voters) {
            if (passed) {
                if (option.contains("day") || option.contains("night")) {
                    sendCmd("/time set " + option);
                } else {
                    sendCmd("/weather " + option);
                }
            } else {
                Bukkit.broadcastMessage("Not enough votes were made!");
            }
        });

        timeVote.start();
    } else {
        $sender.sendMessage("Invalid args: /partyvote weather (option)");
        $sender.sendMessage("Available options: night, day, clear, snow, rain, lightning");
    }
}

worldEnvironmentVote();