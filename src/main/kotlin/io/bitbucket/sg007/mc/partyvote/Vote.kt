package io.bitbucket.sg007.mc.partyvote

import com.okkero.skedule.SynchronizationContext
import com.okkero.skedule.schedule
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class Vote(val plugin: PartyVote, var timer: Int, var desc: String, var onVote: String,
           var required: Double = 100.0, val block: (Boolean, Set<Player>) -> Unit) {

    var active = false
    var result: Boolean? = null
    val voters = mutableSetOf<Player>()
    var votes: Int = 0

    private val commandReminder = PartyVote.MESSAGES.commandReminder
    private var countdownMessage = PartyVote.MESSAGES.countdownTimer

    constructor(plugin: PartyVote, timer: Int, desc: String, block: (Boolean, Set<Player>) -> Unit) : this(plugin, timer, desc, PartyVote.MESSAGES.onVote, 100.0, block)
    constructor(plugin: PartyVote, timer: Int, desc: String, onVote: String, block: (Boolean, Set<Player>) -> Unit) : this(plugin, timer, desc, onVote, 100.0, block)

    fun start() {
        active = true
        if (timer > 45) timer = 45
        if (plugin.placeholders != null) plugin.placeholders!!.vote = this
        plugin.scheduler.schedule(plugin, SynchronizationContext.ASYNC) {
            Bukkit.broadcastMessage(desc.replace("%votes%", "$votes").replace("%online%", "${Bukkit.getOnlinePlayers().size}") + "\n$commandReminder")
            for (i in timer downTo 1) {
                countdownMessage.apply { if (isNotBlank()) Bukkit.broadcastMessage(countdownMessage.replace("%seconds%", "$i")) }
                waitFor(plugin.config.getLong("countdown.speed"))
            }
            val formula: Double = (votes * (required / Bukkit.getOnlinePlayers().size))
            result = (formula >= required)
            block(result!!, voters)
            active = false
        }
    }

    fun castVote(sender: CommandSender) {
        if (sender is Player) {
            if (!hasPermission(sender, PartyVote.PERMISSION_VOTE)) return
            voters.apply {
                if (contains(sender)) {
                    PartyVote.MESSAGES.alreadyVoted.apply {
                        if (isNotEmpty()) forEach { msg -> sender.sendMessage(filterMessage(sender, msg)) }
                    }
                    return
                }
                add(sender)
            }
            votes = voters.size
        } else if (sender is ConsoleCommandSender) {
            println("Console voted")
            votes = +1
        }
        onVote.apply {
            if (isNotEmpty())
                Bukkit.broadcastMessage(filter(onVote))
        }
    }

    private fun filter(string: String): String {
        return string.replace("%votes%", "$votes").replace("%online%", "${Bukkit.getOnlinePlayers().size}")
    }

    override fun toString(): String {
        return "Vote(timer=$timer, desc='$desc', onVote='$onVote', required=$required, active=$active, result=$result, voters=$voters, votes=$votes)"
    }
}

