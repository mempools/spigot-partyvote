package io.bitbucket.sg007.mc.partyvote.script

import io.bitbucket.sg007.mc.partyvote.PartyVote
import io.bitbucket.sg007.mc.partyvote.Script
import java.io.File
import java.io.FileReader
import javax.script.*


class ScriptEnvironment(val plugin: PartyVote) {
    private val scriptManager: ScriptEngineManager = ScriptEngineManager()
    val scripts = mutableMapOf<String, CompiledScript>()
    private val engine: ScriptEngine = scriptManager.getEngineByName("javascript")
    private val context: PluginContext = PluginContext(plugin)

    init {
        plugin.logger.info("Loading Script Environment...")
        engine.getBindings(ScriptContext.GLOBAL_SCOPE).put("\$plugin", plugin)
        engine.getBindings(ScriptContext.GLOBAL_SCOPE).put("\$ctx", context)

        // so i don't  have to make a bootstrap file
        engine.eval("load(\"nashorn:mozilla_compat.js\");" +
                "importPackage(Packages.org.bukkit);" +
                "importPackage(Packages.org.bukkit.command);" +
                "importPackage(Packages.io.bitbucket.sg007.mc.partyvote);")
        loadScripts(plugin)
    }

    private fun loadScripts(plugin: PartyVote) {
        val scriptsDir = File("${plugin.dataFolder}${File.separator}scripts")
        if (!scriptsDir.exists()) scriptsDir.mkdirs()
        val scripts = scriptsDir.listFiles { file -> file.isFile && file.name.contains(".js") }
        scripts?.forEach { file ->
            val script: Script = Script(file.name.replace(".js", ""), file)
            loadScript(script)
            plugin.logger.info(" Loaded [Script=${file.name}]")
        }
    }

    @Throws(ScriptException::class)
    fun loadScript(name: String, bindings: Map<String, Any?>): Boolean {
        val script = scripts[name] ?: return false
        bindings.forEach { key, value -> script.engine.getBindings(ScriptContext.GLOBAL_SCOPE).put(key, value) }
        script.eval()
        return true
    }

    @Throws(ScriptException::class)
    fun loadScript(script: Script) {
        if (scripts.containsKey(script.name)) scripts[script.name]!!.eval() else {
            val compilable = engine as Compilable
            val compiled = compilable.compile(FileReader(script.file))
            scripts.put(script.name, compiled)
        }
    }

    @Throws(ScriptException::class)
    fun reloadScripts() {
        scripts.clear()
        loadScripts(plugin)
    }
}