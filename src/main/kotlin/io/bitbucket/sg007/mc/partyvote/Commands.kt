package io.bitbucket.sg007.mc.partyvote

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class Commands(private val plugin: PartyVote) : CommandExecutor {

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if (sender == null) return true
        if (sender is Player) if (!hasPermission(sender, PartyVote.PERMISSION_VOTE)) return true
        if (args!!.isEmpty()) {
            return plugin.handler.castVote(sender)
        }
        val cmd: String = args[0]
        when (cmd) {
            "list" -> {
                list(sender)
                return true
            }
            "reload", "test" -> {
                return debug(sender, args)
            }
            else -> {
                return plugin.handler.castEvent(sender, cmd, args)
            }
        }
    }

    private fun list(sender: CommandSender) {
        PartyVote.MESSAGES.listEvent.apply { if (isNotBlank()) sender.sendMessage(this) }
        plugin.scriptEnvironment!!.scripts.forEach { name, _ ->
            sender.sendMessage(" $name")
        }
    }

    private fun debug(sender: CommandSender?, args: Array<out String>?): Boolean {
        if (sender is Player && hasPermission(sender, PartyVote.PERMISSION_ADMIN)) return true
        val cmd = args!![0]
        when (cmd) {
            "reload" -> {
                sender!!.sendMessage("Reloading scripts/ ..")
                plugin.scriptEnvironment!!.reloadScripts()
                sender.sendMessage("Reloaded party vote scripts")
            }
            "test" -> plugin.handler.vote = Vote(plugin, 10, "Testing system..",
                    "Thank you for voting, total votes: %votes%",
                    { result, _ ->
                        when (result) {
                            true -> println("PASSED!!!!!")
                            false -> println("FAILED!!!")
                        }
                        Bukkit.broadcastMessage("Votes casted: " + plugin.handler.vote!!.votes)
                    })
        }
        return true
    }
}
