package io.bitbucket.sg007.mc.partyvote.script

import io.bitbucket.sg007.mc.partyvote.PartyVote
import io.bitbucket.sg007.mc.partyvote.Vote
import org.bukkit.Bukkit
import org.bukkit.entity.Player

class PluginContext(val plugin: PartyVote) {
    val handler = plugin.handler
    //temp. wrappers for javascript
    fun setVote(timer: Int, desc: String, block: (Boolean, Set<Player>) -> Unit): Vote {
        handler.vote = Vote(plugin, timer, desc, block)
        return handler.vote!!
    }

    fun setVote(timer: Int, desc: String, voteMsg: String, block: (Boolean, Set<Player>) -> Unit): Vote {
        handler.vote = Vote(plugin, timer, desc, voteMsg, block)
        return handler.vote!!
    }

    fun sendCommand(command: String) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command)
    }

}