package io.bitbucket.sg007.mc.partyvote

import org.bukkit.command.CommandSender

class VoteHandler(val plugin: PartyVote) {

    var vote: Vote? = null

    /**
     * Adds a vote to the current vote session
     * @param sender The player who has voted
     */
    fun castVote(sender: CommandSender): Boolean {
        // if the vote is not active, send help display
        // otherwise, if a vote is in session, cast a vote
        if (vote == null || !vote!!.active) {
            PartyVote.MESSAGES.help.apply { if (isNotEmpty()) forEach { m -> sender.sendMessage(m) } }
            return true
        } else if (vote!!.active)
            vote!!.castVote(sender)
        return true
    }

    /**
     * Prevents multiple events from running. Checks if an event is active before executing another
     * @param sender The player who has voted.
     */
    fun castEvent(sender: CommandSender, cmd: String, args: Array<out String>): Boolean {
        if (vote != null && vote!!.active) {
            PartyVote.MESSAGES.activeVote.apply { if (isNotEmpty()) sender.sendMessage(this) }
            return true
        }
        plugin.scriptEnvironment!!.loadScript(cmd, mapOf(
                "\$sender" to sender, "\$vote" to vote,
                "\$args" to args, "\$size" to args.size))
        return true
    }
}