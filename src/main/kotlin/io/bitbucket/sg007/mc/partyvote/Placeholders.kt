package io.bitbucket.sg007.mc.partyvote

import me.clip.placeholderapi.external.EZPlaceholderHook
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin

class Placeholders(plugin: Plugin?, placeholderName: String = "pv") : EZPlaceholderHook(plugin, placeholderName) {

    var vote: Vote? = null

    override fun onPlaceholderRequest(player: Player?, identifier: String?): String {
        if (identifier.equals("online")) return Bukkit.getOnlinePlayers().size.toString()
        if (vote != null) {
            when (identifier) {
                "description" -> return vote!!.desc
                "votes" -> return "${vote!!.votes}"
                "required" -> "${vote!!.required}"
            }
        }
        if (player == null) return ""
        return ""
    }

}