package io.bitbucket.sg007.mc.partyvote

import me.clip.placeholderapi.PlaceholderAPI
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.io.File

data class Script(var name: String, val file: File)
data class Messages(var noPerm: String = "You do not have permission",
                    var help: List<String> = listOf("===Party Vote Commands ===",
                            "/partyvote - Displays help/Casts a vote during an active event",
                            "/partyvote list - Displays available events",
                            "/partyvote <event name> - Executes an event"
                    ),
                    var alreadyVoted: List<String> = listOf("You've already voted!"),
                    var onVote: String = "%votes%/%online% has voted!",
                    var commandReminder: String = "Type /partyvote to cast a vote!",
                    var countdownTimer: String = "Time left... %seconds%",
                    var activeVote: String = "Please wait for the event to be over",
                    var listEvent: String = "=== List of events ==="
)

fun filterMessage(player: Player?, message: String): String {
    if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
        return PlaceholderAPI.setPlaceholders(player!!, message)
    }
    return message
}


fun hasPermission(player: Player, perm: Permission): Boolean {
    if (!player.hasPermission(perm)) {
        PartyVote.MESSAGES.noPerm.apply { if (isNotEmpty()) player.sendMessage(this) }
    }
    return player.hasPermission(perm)
}