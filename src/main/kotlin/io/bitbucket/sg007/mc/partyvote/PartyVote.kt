package io.bitbucket.sg007.mc.partyvote

import io.bitbucket.sg007.mc.partyvote.script.ScriptEnvironment
import org.bukkit.Bukkit
import org.bukkit.permissions.Permission
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitScheduler
import java.io.File

class PartyVote : JavaPlugin() {

    companion object {
        val PERMISSION_ADMIN = Permission("partyvote.vote", "Permission to cast a vote.")
        val PERMISSION_VOTE = Permission("partyvote.admin", "Permission to admin commands.")
        var MESSAGES: Messages = Messages()
    }

    var scriptEnvironment: ScriptEnvironment? = null
    val scheduler: BukkitScheduler = Bukkit.getScheduler()
    var placeholders: Placeholders? = null
    val handler = VoteHandler(this)

    override fun onEnable() {
        setupPlaceholdersAPI()
        setupConfig()
        scriptEnvironment = ScriptEnvironment(this)
        getCommand("partyvote").executor = Commands(this)
        logger.info("Successfully loaded")
    }

    private fun setupPlaceholdersAPI() {
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            placeholders = Placeholders(this)
            placeholders!!.hook()
        }
    }

    @Throws(Exception::class)
    private fun setupConfig() {
        if (!dataFolder.exists()) dataFolder.mkdirs()
        val file = File(dataFolder, "config.yml")
        if (!file.exists()) {
            logger.info("Creating configuration file...")
            config.apply {
                set("countdown.speed", 2 * 20.toLong())
                set("countdown.message", MESSAGES.countdownTimer)
                set("messages.no-permission", MESSAGES.noPerm)
                set("messages.help", MESSAGES.help)
                set("messages.already-voted", MESSAGES.alreadyVoted)
                set("messages.on-vote", MESSAGES.onVote)
                set("messages.command-reminder", MESSAGES.commandReminder)
                set("messages.list-event", MESSAGES.listEvent)
            }
            config.save(file)
            saveResource("scripts/kick.js", false)
            saveResource("scripts/weather.js", false)
        } else {
            config.load(file)
            MESSAGES.apply {
                noPerm = config.getString("messages.no-permission")
                help = config.getStringList("messages.help")
                alreadyVoted = config.getStringList("messages.already-voted")
                onVote = config.getString("messages.on-vote")
                commandReminder = config.getString("messages.command-reminder")
                countdownTimer = config.getString("countdown.message")
                listEvent = config.getString("messages.list-event")
            }
        }
    }

}
